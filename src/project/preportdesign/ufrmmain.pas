unit ufrmmain;

interface                        

uses                                    
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmbase, frxClass, frxDesgn, frxDBSet, UpDataSource, DB,
  ADODB, ufrmdbbase, ExtCtrls, UPanel, StdCtrls, UpLabel, UpListBox,
  UpComboBox, UpAdoQuery, udatamodule, UpSplitter, Grids, Wwdbigrd,
  Wwdbgrid, Mask, wwdbedit, Wwdotdot, Wwdbcomb, Menus, Buttons,
  UpSpeedButton, umyhisreport, utypes, UpPopupMenu, UpWWDbGrid,
  udbconfigfile, umyconfig, frxChart,ImgList, frxCross, ToolPanels,
  UpAdvToolPanel;

type
  Tfrmmain = class(Tfrmbase)
    ptop: TUPanel;
    lbl1: TUpLabel;
    pleft: TUPanel;
    qry: TUpAdoQuery;
    qrytemp: TUpAdoQuery;
    pclient: TUPanel;
    upspltr1: TUpSplitter;
    upspltr2: TUpSplitter;
    qrydataset: TUpAdoQuery;
    qryvariable: TUpAdoQuery;
    dsdateset: TUpDataSource;
    dsvariable: TUpDataSource;
    pmdataset: TPopupMenu;
    pmnSqlValid: TMenuItem;
    btndesign: TUpSpeedButton;
    btnquit: TUpSpeedButton;
    frpreport: TfrxReport;
    frxdbds_m: TfrxDBDataset;
    frxdbds_d: TfrxDBDataset;
    frxdsgnr1: TfrxDesigner;
    qryreport_m: TUpAdoQuery;
    qryreport_d: TUpAdoQuery;
    frxchrtbjct1: TfrxChartObject;
    frxcrsbjct1: TfrxCrossObject;
    p2: TUpAdvToolPanel;
    cbbmok: TUpComboBox;
    p3: TUpAdvToolPanel;
    lstform: TUpListBox;
    p4: TUpAdvToolPanel;
    lstreport: TUpListBox;
    p5: TUpAdvToolPanel;
    p12: TUPanel;
    btn1: TUpSpeedButton;
    btn2: TUpSpeedButton;
    dbgdataset: TUpWWDbGrid;
    cbbdayjlksd: TwwDBComboBox;
    cbbdayjljsd: TwwDBComboBox;
    p6: TUpAdvToolPanel;
    dbgvariable: TUpWWDbGrid;
    frxdbdtst_3: TfrxDBDataset;
    frxdbdtst_4: TfrxDBDataset;
    frxdbdtst_5: TfrxDBDataset;
    qryreport_3: TUpAdoQuery;
    qryreport_4: TUpAdoQuery;
    qryreport_5: TUpAdoQuery;
    procedure cbbmokChange(Sender: TObject);
    procedure lstformDblClick(Sender: TObject);
    procedure lstreportDblClick(Sender: TObject);
    procedure pmdatasetPopup(Sender: TObject);
    procedure pmnSqlValidClick(Sender: TObject);
    procedure btnquitClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure frpreportGetValue(const VarName: String; var Value: Variant);
    procedure btndesignClick(Sender: TObject);
  private     
    mainconfig : TMyConfig;
    //系统主配置文件myhis.ini
    
    procedure fillforms(mokbm : string);
    //根据模块填充报表
    procedure fillreports(chuangtmc : string);
    //根据窗体名称填充报表
    procedure filldatasets(baobbm : integer);
    //根据报表编码填充数据集集合
    procedure fillvariables(baobbm : integer);
    //根据报表编码填充变量集合

    function ReportDS_bianm2baobmc(bianm : integer) : String;
    //报表编码2报表名称
    function ReportDS_bianm2baobwjmc(bianm : integer) : String;
    //根据报表编码获取报表文件名称
    function connectdb() : Boolean; 
  public
    fdbserver : string;
    fdbuser : string;
    fdbpassword : string;
    fdbdatabase : string;
    
    function init() : Boolean;
    //初始化
  
  end;

var
  frmmain: Tfrmmain;

implementation

{$R *.dfm}

{ Tfrmmain }

function Tfrmmain.init: Boolean;
var
  i : integer;
  str, strconfigfile : string;
begin
  Result := False;

  //连接数据库
  if not connectdb() then Exit;

  { 数据集控件初始化
  }
  for i:=0 to self.ComponentCount-1 do
  begin
    if Components[i] is TUpAdoQuery then
    begin
      (Components[i] as TUpAdoQuery).Close;
      (Components[i] as TUpAdoQuery).Connection := dm.conn;
    end;
  end;

  { 模块列表初始化
  }
  cbbmok.ClearItems;
  cbbmok.additemup('查询报表','cxbb');

  //不允许编辑数据集和变量集dataset
  dbgdataset.ReadOnly := True;
  dbgvariable.ReadOnly := True;
    
  Result := True;
end;

procedure Tfrmmain.cbbmokChange(Sender: TObject);
begin
  inherited;

  { 模块变化, 刷新模块下窗体
  }
  fillforms('');
  fillreports('');
  filldatasets(0);
  fillvariables(0);
            
  //不允许编辑数据集和变量集dataset
  dbgdataset.ReadOnly := True;
  dbgvariable.ReadOnly := True;
  
  if cbbmok.ItemIndex <> -1 then
    fillforms(cbbmok.GetItemValue);
end;

procedure Tfrmmain.fillforms(mokbm: string);
begin
  qry.Close;
  qry.SQL.Text := 'select chuangtmc, chuangtms from jc_baobdy' +
      ' where mokbm=''' + mokbm + '''';
  qry.Open;
  lstform.ClearItems;
  while not qry.Eof do
  begin
    if lstform.ValueItemIndex(qry.fieldbyname('chuangtmc').AsString) = -1 then
    begin
      lstform.AddItemUP(qry.fieldbyname('chuangtms').AsString,
        qry.fieldbyname('chuangtmc').AsString);
    end;

    qry.Next;
  end;
  qry.Close;
end;

procedure Tfrmmain.lstformDblClick(Sender: TObject);
begin
  inherited;
  { 双击窗体, 刷新报表列表
  }
  fillreports('');
  filldatasets(0);
  fillvariables(0);
            
  //不允许编辑数据集和变量集dataset
  dbgdataset.ReadOnly := True;
  dbgvariable.ReadOnly := True;
  
  if lstform.ItemIndex <> -1 then
    fillreports(lstform.GetItemValue);
end;

procedure Tfrmmain.fillreports(chuangtmc: string);
begin
  lstreport.ClearItems;

  qry.Close;
  qry.SQL.Text := 'select bianm, baobmc, baobms from jc_baobdy' +
      ' where chuangtmc=''' + chuangtmc + '''';
  qry.Open;
  while not qry.Eof do
  begin
    if lstreport.ValueItemIndex(qry.fieldbyname('bianm').AsString) = -1 then
    begin
      lstreport.AddItemUP(qry.fieldbyname('baobms').AsString,
        qry.fieldbyname('bianm').AsString);
    end;

    qry.Next;
  end;
  qry.Close;
end;

procedure Tfrmmain.lstreportDblClick(Sender: TObject);
begin
  inherited;
  filldatasets(0);
  fillvariables(0);

  //不允许编辑数据集和变量集dataset
  dbgdataset.ReadOnly := True;
  dbgvariable.ReadOnly := True;

  if lstreport.ItemIndex <> -1 then
  begin
    //填充报表数据集集合
    filldatasets(StrToInt(lstreport.GetItemValue));

    //填充报表变量集合
    fillvariables(StrToInt(lstreport.GetItemValue));

    //允许编辑数据集/变量集dbg
    dbgdataset.ReadOnly := False;
    //dbgvariable.ReadOnly := False;
  end;
end;

procedure Tfrmmain.filldatasets(baobbm: integer);
begin
  qrydataset.Close;
  qrydataset.SQL.Text := 'select * from jc_baobsjj where baobbm=' +
      IntToStr(baobbm);
  qrydataset.Open;
end;

procedure Tfrmmain.fillvariables(baobbm: integer);
begin
  qryvariable.Close;
  qryvariable.SQL.Text := 'select * from jc_baobbl where baobbm=' +
      IntToStr(baobbm);
  qryvariable.Open;
end;

procedure Tfrmmain.pmdatasetPopup(Sender: TObject);
begin
  inherited;
  if (not qrydataset.Active) or (qrydataset.RecordCount = 0) then
  begin
    pmnSqlValid.Enabled := False;
  end
  else
  begin
    pmnSqlValid.Enabled := True;
  end;
end;

procedure Tfrmmain.pmnSqlValidClick(Sender: TObject);
var
  index : Integer;
  strsql : string;
  substr : string;
begin
  inherited;
  
  try
    strsql := UpperCase(qrydataset.fieldbyname('shujjsql').AsString);
    index := Pos('SELECT', strsql);
    if index = 0 then
    begin
      raise Exception.Create('预期的select未找到！');
    end;
    substr := Copy(strsql, index+6, Length(strsql)-index-6+1);
    qrytemp.Close;
    qrytemp.SQL.Text := 'select top 1 ' + substr;
    qrytemp.Open;
    qrytemp.Close;
    baseobj.showmessage('sql语法检查通过！');
  except
    on E:Exception do
    begin
      baseobj.showerror('Sql语法检查不通过：' + e.Message);
    end;
  end;
end;

procedure Tfrmmain.btnquitClick(Sender: TObject);
begin
  inherited;

  Close();
end;

procedure Tfrmmain.btn1Click(Sender: TObject);
begin
  inherited;
  qrydataset.UpdateBatch();
end;

procedure Tfrmmain.btn2Click(Sender: TObject);
begin
  inherited;

  qrydataset.CancelBatch();
end;

procedure Tfrmmain.frpreportGetValue(const VarName: String; var Value: Variant);
var
  report : TMyHisReport;
  variable : TMyHisReportVariable;
begin
  // 报表变量值处理
  if qryvariable.Locate('bianlmc', VarName, []) then
  begin
    Value := 'testvalue';
  end;
end;

procedure Tfrmmain.btndesignClick(Sender: TObject);
var
  report_filename : string;
  i : Integer;
  pqry_temp : ^TUpADOQuery;
  pfrxdbds: PfrxDBDataset;
  baobwjmc : String;
begin
  //查找当前报表是否定义;
  if lstreport.ItemIndex = -1 then
  begin
    baseobj.showwarning('请选择一个具体的报表！');
    Exit;
  end;

  baobwjmc := ReportDS_bianm2baobwjmc(StrToInt(lstreport.GetItemValue));
  if baobwjmc = '' then
  begin
    //合成报表文件路径
    report_filename := ExtractFilePath(Application.ExeName) +
        '\reports\' + lstform.GetItemValue + '_' +
        ReportDS_bianm2baobmc(StrToInt(lstreport.GetItemValue)) + '.fr3';
  end
  else
  begin
    //合成报表文件路径
    report_filename := ExtractFilePath(Application.ExeName) +
        '\reports\' + baobwjmc + '.fr3';
  end;
  
  if not FileExists(report_filename) then
  begin
    baseobj.showerror('报表文件' + report_filename + '未找到！');
    Exit;
  end;

  //加载报表
  frpreport.LoadFromFile(report_filename);

  //加载报表变量
  frpreport.Variables.Clear;
  qryvariable.First;
  frpreport.Variables.Add.Name := ' 参数';
  while not qryvariable.Eof do
  begin
    frpreport.Variables.AddVariable('参数',
        qryvariable.fieldbyname('bianlmc').AsString,
        qryvariable.fieldbyname('bianlms').AsString);
        
    qryvariable.Next;
  end;

  //清空报表数据集集合
  frpreport.DataSets.Clear;
  qrydataset.First;
  i := 0;
  while not qrydataset.Eof do
  begin
    pqry_temp := nil;
    pfrxdbds := nil;
    case i of
      0:
      begin  //第一个数据集
        pfrxdbds := @frxdbds_m;
        pqry_temp := @qryreport_m;

        //根据报表sql打开数据集
        qryreport_m.Close;
        qryreport_m.SQL.Text := qrydataset.fieldbyname('shujjsql').AsString;
        qryreport_m.Open;
      end;
      1:
      begin  //第二个数据集
        pfrxdbds := @frxdbds_d;   
        pqry_temp := @qryreport_d;
        
        //根据报表sql打开数据集
        qryreport_d.Close;
        qryreport_d.SQL.Text := qrydataset.fieldbyname('shujjsql').AsString;
        qryreport_d.Open;
      end;
      2:
      begin
        pfrxdbds := @frxdbdtst_3;
        pqry_temp := @qryreport_3;
        
        //根据报表sql打开数据集
        qryreport_3.Close;
        qryreport_3.SQL.Text := qrydataset.fieldbyname('shujjsql').AsString;
        qryreport_3.Open;
      end;
      3:
      begin
        pfrxdbds := @frxdbdtst_4;
        pqry_temp := @qryreport_4;

        //根据报表sql打开数据集
        qryreport_4.Close;
        qryreport_4.SQL.Text := qrydataset.fieldbyname('shujjsql').AsString;
        qryreport_4.Open;
      end;
      4:
      begin
        pfrxdbds := @frxdbdtst_5;
        pqry_temp := @qryreport_5;

        //根据报表sql打开数据集
        qryreport_5.Close;
        qryreport_5.SQL.Text := qrydataset.fieldbyname('shujjsql').AsString;
        qryreport_5.Open;
      end;
      else
      begin
        baseobj.showwarning('报表定义的数据集个数超过了最大个数,打印结果可能不正确！');
      end;
    end;
    i := i + 1;
    
    if pfrxdbds <> nil then
    begin
      pfrxdbds^.DataSet := pqry_temp^;
      pfrxdbds^.UserName := qrydataset.fieldbyname('shujjyhmc').AsString;
      pfrxdbds^.Description := qrydataset.fieldbyname('shujjms').AsString;
      //打印记录开始点设置
      case qrydataset.fieldbyname('dayjlksd').AsInteger of
        1: //开始
        begin
          pfrxdbds^.RangeBegin := rbFirst;
        end;
        2: //当前
        begin
          pfrxdbds^.RangeBegin := rbCurrent;
        end;
        else
        begin
          baseobj.showwarning('数据集打印记录开始点设置错误,打印可能不正确!');
        end;
      end;         
      //打印记录结束点设置
      case qrydataset.fieldbyname('dayjljsd').AsInteger of
        2: //当前
        begin
          pfrxdbds^.RangeEnd := reCurrent;
        end;
        3:
        begin
          pfrxdbds^.RangeEnd := reLast;
        end;
        4:
        begin
          pfrxdbds^.RangeEnd := reCount;
        end;
        else
        begin
          baseobj.showwarning('数据集打印记录开始点设置错误,打印可能不正确!');
        end;
      end;
      //打印记录条数
      pfrxdbds^.RangeEndCount := qrydataset.fieldbyname('dayjlts').AsInteger;

      frpreport.DataSets.Add(pfrxdbds^);
    end;

    qrydataset.Next;
  end;

  frpreport.DesignReport();
end;

function Tfrmmain.ReportDS_bianm2baobmc(bianm: integer): String;
begin
  Result := '';
  qry.Close;
  qry.SQL.Text := 'select baobmc from jc_baobdy where bianm=' + IntToStr(bianm);
  qry.Open;
  if qry.RecordCount <> 0 then
    Result := qry.fieldbyname('baobmc').AsString;
  qry.Close;
end;

function Tfrmmain.connectdb: Boolean;
begin
  try
    if dm.conn.Connected then dm.conn.Close;

    dm.conn.ConnectionString := 'Provider=SQLOLEDB.1; ' +
        'Password=' + fdbpassword + '; ' +
        'Persist Security Info=True; ' +
        'User ID=' + fdbuser + '; ' +
        'Initial Catalog=' + fdbdatabase + '; ' +
        'Data Source=' + fdbserver;
    dm.conn.Open();
    
    Result := True;
  except
    on E:Exception do
    begin
      errmsg := '连接数据库失败:' + e.Message;    
      Result := False;
    end; 
  end;
end;

function Tfrmmain.ReportDS_bianm2baobwjmc(bianm: integer): String;
begin       
  Result := '';
  exit;
  qry.OpenSql('select baobwjmc from jc_baobdy where bianm=' + IntToStr(bianm));
  if qry.getString('baobwjmc','') <> '' then
  begin
    Result := qry.getString(0);
  end;
  qry.Close;
end;

end.
