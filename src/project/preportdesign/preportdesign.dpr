program preportdesign;

uses
  Forms,
  Windows,
  Messages,
  CnHint,
  SysUtils,
  ufrmbase in '..\lib\formbase\ufrmbase.pas' {frmbase},
  HzSpell in '..\lib\moudle\huoqupinyin\HzSpell.pas',
  ubase in '..\lib\common\ubase.pas',
  udbbase in '..\lib\common\udbbase.pas',
  udbconfigfile in '..\lib\common\udbconfigfile.pas',
  uencode in '..\lib\common\uencode.pas',
  uglobal in '..\lib\common\uglobal.pas',
  ukeyvaluelist in '..\lib\common\ukeyvaluelist.pas',
  umyconfig in '..\lib\common\umyconfig.pas',
  umysystem in '..\lib\common\umysystem.pas',
  utypes in '..\lib\common\utypes.pas',
  uDmBase in '..\lib\datamoudle\uDmBase.pas' {DMBase: TDataModule},
  udmtblbase in '..\lib\datamoudle\udmtblbase.pas' {dmtblbase: TDataModule},
  utblyuang in '..\classes\dbtableclass\utblyuang.pas',
  udlltransfer in '..\dllmoudle\dll_interfaces\udlltransfer.pas',
  udbcheck in '..\lib\moudle\dbcheck\udbcheck.pas',
  ufrmdbbase in '..\lib\formbase\ufrmdbbase.pas' {frmdbbase},
  udmtbl_public in '..\classes\common\udmtbl_public.pas' {dmtbl_public: TDataModule},
  umyhisreport in '..\classes\report\umyhisreport.pas',
  udatamodule in 'datamoudle\udatamodule.pas' {dm: TDataModule},
  ufrmcxbb1DbgridbaseMDI in '..\lib\formbase\ufrmcxbb1DbgridbaseMDI.pas' {frmcxbb1DbgridbaseMDI},
  ufrmcxbb2DbgridbaseMDI in '..\lib\formbase\ufrmcxbb2DbgridbaseMDI.pas' {frmcxbb2DbgridbaseMDI},
  ufrmcxbbbaseMDI in '..\lib\formbase\ufrmcxbbbaseMDI.pas' {frmcxbbbaseMDI},
  ufrmdbdialogbase in '..\lib\formbase\ufrmdbdialogbase.pas' {frmdbdialogbase},
  ufrmdbeditbase in '..\lib\formbase\ufrmdbeditbase.pas' {frmdbeditbasae},
  ufrmdbgridbase in '..\lib\formbase\ufrmdbgridbase.pas' {frmdbgridbase},
  ufrmdbnavbase in '..\lib\formbase\ufrmdbnavbase.pas' {frmdbnavbase},
  ufrmdbselectbase in '..\lib\formbase\ufrmdbselectbase.pas' {frmdbselectbase},
  ufrmdbtree_selbase in '..\lib\formbase\ufrmdbtree_selbase.pas' {frmdbtree_selbase},
  ufrmdbtreebase in '..\lib\formbase\ufrmdbtreebase.pas' {frmdbtreebase},
  ufrmmdichildbase in '..\lib\formbase\ufrmmdichildbase.pas' {frmmdichildbase},
  udbtree in '..\lib\moudle\dbtree\udbtree.pas',
  ufrmcomm_showmessage in '..\dllmoudle\common\ufrmcomm_showmessage.pas' {frmcomm_showmessage},
  udllfuns_dbgridSelfDraw in '..\dllmoudle\dll_interfaces\udllfuns_dbgridSelfDraw.pas',
  ucsmsg in '..\classes\csmsg\ucsmsg.pas',
  ufrmmain in 'ufrmmain.pas' {frmmain},
  PerlRegEx in '..\lib\moudle\TPerlRegEx\PerlRegEx.pas',
  pcre in '..\lib\moudle\TPerlRegEx\pcre.pas';

{$R *.res}

var
  hGlobalMutex : THandle;
  dwGlobalMutex_return : DWORD;

begin
  try      
    { 防止重复启动控制, 通过创建有名信号量方式
    }
    hGlobalMutex := CreateMutex(nil, false, 'pmyhisreportdesign');
    dwGlobalMutex_return := GetLastError;
    if ERROR_ALREADY_EXISTS = dwGlobalMutex_return then
    begin
      Application.MessageBox('报表设计器已启动！', '系统提示');
      Sleep(1000);
      Application.Terminate;
      Exit;
    end;
    
    Application.Initialize;    
    Application.CreateForm(Tfrmmain, frmmain);
  Application.CreateForm(Tdm, dm);
  if ParamCount <> 4 then
    begin
      raise Exception.Create('非法的参数调用！');
    end;
    frmmain.fdbserver := ParamStr(1);
    frmmain.fdbuser := ParamStr(2);
    frmmain.fdbpassword := ParamStr(3);
    frmmain.fdbdatabase := ParamStr(4);
                            
    {
    frmmain.fdbserver := '(Local)\GSQL';
    frmmain.fdbuser := 'sa';
    frmmain.fdbpassword := 'admin';
    frmmain.fdbdatabase := 'yusgl'; 
    }
    if not frmmain.init() then
    begin
      raise Exception.Create('报表设计器初始化失败:' + frmmain.errmsg);
    end;

    Application.Run;
  except
    on E:Exception do
    begin
      if Assigned(dm) then
        FreeAndNil(dm);

      frmmain.baseobj.showerror(E.Message);

      if Assigned(frmmain) then
        FreeAndNil(frmmain);
        
      Application.Terminate;
      Exit;
    end;
  end;
end.
