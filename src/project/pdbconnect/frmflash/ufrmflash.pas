unit ufrmflash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmbase, ExtCtrls, StdCtrls;

type
  Tfrmflash = class(Tfrmbase)
    pclient: TPanel;
    pbottom: TPanel;
    imgflash: TImage;
    lblmsg: TLabel;
  private
    { Private declarations }
  public
    procedure setmsg(strmsg : string);
    //设置提示信息
    
  end;

var
  frmflash: Tfrmflash;

implementation

{$R *.dfm}

{ Tfrmflash }

procedure Tfrmflash.setmsg(strmsg: string);
begin
  lblmsg.Caption := strmsg;
  application.ProcessMessages;
end;

end.
