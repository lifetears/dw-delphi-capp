unit udllfunctions;

interface   
uses     
  SysUtils, Classes, Forms, Dialogs, DB, ADODB, udlltransfer,
  uglobal, utypes, shellapi, windows, ufrmxitgl_cansshez, rzpanel;
        
{ 菜单总体调度
}
procedure MakeDllParams(dllparam : PDllParam);
function frmmain_menuitem_click(dllparam : PDllParam; menuid : integer) : PForm;

implementation

uses
  ufrmxitgl_quanxgl, ufrmxitgl_xiugkoul;


{ 调用dll参数生成函数
}
procedure MakeDllParams(dllparam : PDllParam);
begin
  DllParam^.papp := @Application;
  dllparam^.padoconn := gmysystem.fpadocon;
  DllParam^.pscreen := @Screen;
  dllparam^.mysystem := @gmysystem;
end;
                  
function getDbLoingIngo(connectionstring : string;
    var dbserver, dbuser, dbpassword, dbdatabase : string) : Boolean;
var
  i, p : Integer;
  strupperconnstring :string;
begin
  Result := False;
  dbserver := '';
  dbuser := '';
  dbpassword := '';
  dbdatabase := '';    
  strupperconnstring := UpperCase(connectionstring);
  //Provider=SQLOLEDB.1;
  //Password=12345;
  //Persist Security Info=True;
  //User ID=sa;
  //Initial Catalog=uphis;
  //Data Source=.

  //get password
  i := Pos(UpperCase('Password='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('Password=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbpassword := Copy(connectionstring, i, p-i);

  //get userid
  i := Pos(UpperCase('User ID='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('User ID=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbuser := Copy(connectionstring, i, p-i);

  //get dbdatabase
  i := Pos(UpperCase('Initial Catalog='), strupperconnstring);
  if i = 0 then Exit;
  i := i + Length('Initial Catalog=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbdatabase := Copy(connectionstring, i, p-i);

  //get dbserver
  i := Pos(UpperCase('Data Source='), strupperconnstring);
  if i = 0 then Exit;  
  i := i + Length('Data Source=');
  for p:=i to Length(strupperconnstring) do
  begin
    if strupperconnstring[p] = ';' then
      Break;
  end;
  dbserver := Copy(connectionstring, i, p-i);

  Result := True;
end;


{////////////////////////系统管理部分函数///////////////////////////////
}                              
//参数设置
function cansshez(dllparams : PDllParam) : PForm;
begin
  if not assigned(frmxitgl_cansshez) then
    frmxitgl_cansshez := tfrmxitgl_cansshez.Create(nil);
  frmxitgl_cansshez.Execute_dll_show(dllparams);

  result := @frmxitgl_cansshez;
end;

//修改口令
function xiugkoul(dllparams : PDllParam) : PForm;
begin   
  try                
    if not Assigned(frmxitgl_xiugkoul) then
      frmxitgl_xiugkoul := Tfrmxitgl_xiugkoul.Create(nil);
    frmxitgl_xiugkoul.Execute_dll_showmodal(dllparams);
  finally
    frmxitgl_xiugkoul.Free;
    frmxitgl_xiugkoul := nil;
  end;
  
  Result := nil;
end;

//权限管理
function quanxguanl(dllparams : PDllParam) : PForm;
begin
  if not assigned(frmxitgl_quanxgl) then
    frmxitgl_quanxgl := tfrmxitgl_quanxgl.Create(nil);
  frmxitgl_quanxgl.Execute_dll_show(dllparams);

  result := @frmxitgl_quanxgl;
end;

//系统操作日志查询 
function cxbb_xitgl_caozrz(dllparams : PDllParam) : PForm;
begin
end;

//系统接口日志查询 
function cxbb_xitgl_jiekrz(dllparams : PDllParam) : PForm;
begin
end;


{////////////////////////基础数据部分函数///////////////////////////////
}


{////////////////////////业务管理部分函数///////////////////////////////
}


//系统管理
function frmmain_menuitem_click_xitgl(dllparam : PDllParam; menuid : integer):PForm;
var
  dbserver, dbuser, dbpassword, dbdatabase : string;
  reportdesignapp_path : string;
begin
  Result := nil;
  
  case menuid of 
    //业务办理
    1001: //参数设置
    begin
      result := cansshez(dllparam);
    end;
    1002: //修改口令
    begin
      result := xiugkoul(dllparam);
    end;
    1003: //权限管理
    begin
      result := quanxguanl(dllparam);
    end;
    1004: //报表设计
    begin
      if not getDbLoingIngo(dllparam^.padoconn.ConnectionString, dbserver,
        dbuser, dbpassword, dbdatabase) then
      begin
        Exit;
      end;

      reportdesignapp_path := ExtractFilePath(application.ExeName) + '\' +
        'preportdesign.exe';
      ShellExecute(0,'open',PChar(reportdesignapp_path),
        PChar(dbserver + ' ' + dbuser + ' ' + dbpassword + ' ' +
          dbdatabase), '', SW_SHOW);
    end;
    1101: //系统操作日志查询
    begin
      result:= cxbb_xitgl_caozrz(dllparam);
    end;
    1102: //系统接口日志查询
    begin
      result:= cxbb_xitgl_jiekrz(dllparam);
    end;
  end;
end;
          
//基础数据
function frmmain_menuitem_click_jicsj(dllparam : PDllParam; menuid : integer):PForm;
begin        
  Result := nil;
  
  case menuid of
    //业务办理
    1201: //专业设置
    begin
      //result := zhuanyeshezhi(dllparam);
    end;
  end;
end;

//数据字典
function frmmain_menuitem_click_shujzd(dllparam : PDllParam; menuid : integer):PForm;
begin        
  Result := nil;
  
  case menuid of
    //业务办理
    1301: //检查项管理
    begin
      //result := jiancxgl(dllparam);
    end;
  end;
end;

//业务管理
function frmmain_menuitem_click_yewgl(dllparam : PDllParam; menuid : integer):PForm;
begin
  Result := nil;
  
  case menuid of
    //业务办理
    1401: //评审管理员
    begin
      //result := pingszx_guanly(dllparam);
    end;
  end;
end;

{ 主界面菜单项单击处理事件
}
function frmmain_menuitem_click(dllparam : PDllParam; menuid : integer) : PForm;
begin   
  //这个大函数根据menuid调度不同的实际处理函数     
  Result := nil;
  
  //系统管理
  if ((menuid >=1001) and (menuid<1200)) then
  begin
    result := frmmain_menuitem_click_xitgl(dllparam, menuid);
  end
  //基础数据
  else if((menuid>=1200) and (menuid<1300)) then
  begin
    result := frmmain_menuitem_click_jicsj(dllparam, menuid);
  end
  //评审模板制作
  else if((menuid>=1300) and (menuid<1400)) then
  begin
    result := frmmain_menuitem_click_shujzd(dllparam, menuid);
  end
  //业务管理
  else if((menuid>=1400) and (menuid<1600)) then
  begin
    result := frmmain_menuitem_click_yewgl(dllparam, menuid);
  end
end;

end.

