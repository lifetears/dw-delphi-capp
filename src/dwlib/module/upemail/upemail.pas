{********************************************************************
  email控制类
       myhis

       版权所有 (C) 2013 upsof
修改历史：
  2013-01-27: 实现基本收取英文的subject；满足myhis auth需求即可；
  
********************************************************************}


unit upemail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, IdMessage, IdSMTP,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient,
  IdPOP3, IdMessageCoder, IdMessageCoderMIME, IdCoder, IdCoder3to4,
  IdCoderMIME, IdCoderUUE, IdCoderXXE, IdCoderQuotedPrintable, udbbase,
  UpAdoConnection, utypes;

type
  { email控制类 }
  TUPEmail = class(TDbBase)

  private
    fHostAddress : string;
    fUserName : string;
    fUserPassword : string;
    ftimeout : Integer;
    fsmtp_host : string;
    fsmtp_username : string;
    fsmtp_userpassword : string;
    fsmtp_port : string;

    fidpop3: TIdPOP3;
    fidmsg: TIdMessage;
    fidsmtp : TIdSMTP;
    ffromemail : string; //邮件来自于地址
    ftoemail : string; //邮件发送到地址
    femail_subject : string; //邮件标题
    femail_body : string; //邮件正文
  public
    errcode : Integer;
    errmsg : string;

  public
    function connect() : Boolean;
    //连接邮箱
    function disconnect() : Boolean;
    //断开连接

    function getemailcount(var icount : integer) : Boolean;
    //取邮件份数
    function getsubjectEN(inum : integer; var strsubject : string) : Boolean;
    //取第inum份邮件的subject

    function sendemail() : Boolean;
    //发送邮件
    procedure emailBodyClear();
    procedure addEmailBodyString(str : string);

  public
    procedure myhisauthinfo_unpack(subject : string; var userid, UserName, command : string);
  public
    constructor Create(padocon : PUpAdoConnection);
    destructor Destroy();
  published
    property hostname : string read fhostaddress write fHostAddress;
    property username : string read fUserName write fUserName;
    property userpassword : string read fuserpassword write fuserpassword;
    property timeout : integer read ftimeout write ftimeout;
    property smtp_host : string read fsmtp_host write fsmtp_host;
    property smtp_username : string read fsmtp_username write fsmtp_username;
    property smtp_userpassword : string read fsmtp_userpassword write fsmtp_userpassword;
    property smtp_port : string read fsmtp_port write fsmtp_port;
    property fromemail : string read ffromemail write ffromemail;
    property toemail : string read ftoemail write ftoemail;
    property email_subject : string read femail_subject write femail_subject;
    property email_body : string read femail_body write femail_body;
  end;

implementation

{ TUPEmail }

procedure TUPEmail.addEmailBodyString(str: string);
begin
  fidmsg.Body.Add(str);
end;

function TUPEmail.connect: Boolean;
begin
  if fidpop3.Connected then
    fidpop3.Disconnect;

  try
    fidpop3.Host := fHostAddress;
    fidpop3.Username := fUserName;
    fidpop3.Password := fUserPassword;
    fidpop3.Connect(ftimeout);
    Result := True;
  except
    on E:Exception do
    begin
      errmsg := '连接失败:' + e.Message;
      Result := False;
    end;
  end;
end;

constructor TUPEmail.Create(padocon : PUpAdoConnection);
begin
  inherited Create(padocon);
  
  fidpop3 := TIdPOP3.Create(nil);
  fidmsg := TIdMessage.Create(nil);
  fidsmtp := TIdSMTP.Create(nil);
  ftimeout := 1000;
end;

destructor TUPEmail.Destroy;
begin
  if fidpop3.Connected then
    fidpop3.Disconnect;
  FreeAndNil(fidpop3);
  FreeAndNil(fidmsg);
  FreeAndNil(fidsmtp);
  
  inherited Destroy();
end;

function TUPEmail.disconnect: Boolean;
begin
  fidpop3.Disconnect;
end;

procedure TUPEmail.emailBodyClear;
begin
  fidmsg.Body.Clear;
end;

function TUPEmail.getemailcount(var icount: integer): Boolean;
begin  
  Result := False;

  if not fidpop3.Connected then
  begin
    errmsg := '尚未连接';
    Exit;
  end;

  icount := fidpop3.CheckMessages;

  Result := True;
end;

function TUPEmail.getsubjectEN(inum: integer; var strsubject: string): Boolean;
begin
  try
    if not fidpop3.Connected then
      raise Exception.Create('尚未连接');

    fidmsg.Clear;
    fidpop3.RetrieveHeader(inum, fidmsg);
    strsubject := fidmsg.Subject;
    
    Result := True;
  except
    on E:Exception do
    begin
      errmsg := '取信失败:' + e.message;
      Result := False;
    end;
  end;
end;

procedure TUPEmail.myhisauthinfo_unpack(subject: string; var userid,
  UserName, command: string);
var
  ipos : Integer;
  str, strhead : string;
begin
  userid := '';
  UserName := '';
  command := '';

  //head
  ipos := Pos('_', subject);
  if ipos = 0 then Exit;
  strhead := Copy(subject, 1, ipos-1);
  if strhead <> 'myhisauth' then Exit;
  
  str := Copy(subject, ipos+1, Length(subject)-ipos);
  //userid
  ipos := Pos('_', str);
  if ipos = 0 then Exit;
  userid := Copy(str, 1, ipos-1);
  str := Copy(str, ipos+1, Length(str)-ipos);
  //username
  ipos := Pos('_', str);
  if ipos = 0 then Exit;
  UserName := Copy(str, 1, ipos-1);
  str := Copy(str, ipos+1, Length(str)-ipos);
  //command
  command := str;
end;

function TUPEmail.sendemail: Boolean;
begin
  try
    if fidsmtp.Connected then
      fidsmtp.Disconnect;

    fidsmtp.AuthenticationType := atLogin;
    fidsmtp.Host := fsmtp_host;
    fidsmtp.Port := StrToInt(fsmtp_port);
    fidsmtp.Username := fsmtp_username;
    fidsmtp.Password := fsmtp_userpassword;
    fidsmtp.Connect(timeout);

    //fidmsg.Clear;
    fidmsg.Subject := femail_subject;
    //fidmsg.Body.Add(femail_body);
    fidmsg.From.Address := ffromemail;
    fidmsg.Recipients.EMailAddresses := ftoemail;
    fidsmtp.Send(fidmsg);
    fidsmtp.Disconnect;

    Result := True;
  except
    on E:Exception do
    begin
      errmsg := e.Message;
      if fidsmtp.Connected then
        fidsmtp.Disconnect;
      Result := False;
    end;
  end;
end;

end.
