
//*******************************************************************
//  pmyhisgroup  ptblmanage   
//  udmtblbase.pas     
//                                          
//  创建: changym by 2012-03-02 
//        changup@qq.com                    
//  功能说明:                                            
//      表实体基类;
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************
unit udmtblbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uDmBase, ADODB, UpAdoStoreProc, DB, UpAdoQuery, udlltransfer,
  ubase, udbcheck, utypes;

type
  { 存储过程参数
  }
  PSPParam = ^TSPParam;
  TSPParam = record
    name : string;
    value : Variant;
  end;

  //表对象指针
  Pdmtblbase = ^Tdmtblbase;
  Tdmtblbase = class(TDMBase)
    spcall: TUpAdoStoreProc;
    qrylog: TUpAdoQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);   
  private
    FAppendSpName : string;
    FUpdateSpName : string;
    FDeleteSpName : string;  
  private
    procedure setAppendSpName(spname : string);
    procedure setUpdateSpName(spname : string);
    procedure setDeleteSpName(spname : string);
  protected
    baseobj : tbase;
    //基础类对象
    dbcheck : tdbcheck;
    //数据库检查对象
  protected
    spparam_in : TList;
    //存储过程输入参数
    spparam_out : TList;
    //存储过程输出参数
  protected
    procedure appendcheck(); virtual;
    //新增前的检查大多数在展现层就完成了
    procedure appendbefore(); virtual;
    procedure appendpost(); virtual;
    procedure appendafter(); virtual;
    //子类实现,主要用于取存储过程返回参数

    procedure updatecheck(); virtual;
    procedure updatebefore(); virtual;
    procedure updatepost(); virtual;
    procedure updateafter(); virtual;
    //子类实现,主要用于取存储过程返回参数

    procedure deletecheck(); virtual;
    procedure deletebefore(); virtual;
    procedure deletepost(); virtual;
    procedure deleteafter(); virtual;
    //子类实现,主要用于取存储过程返回参数

  public
    //20180120:将此函数公开，子类不再编写单独的专门的处理函数
    procedure ExecuteSProedure(psp : PUpAdoStoreProc; bWithBianm : Boolean = False); virtual;
    //按照框架规则执行指定的存储过程
    
  public
    constructor Create(pdllparams : PDllParam); override;
    destructor Destroy; override;

  public
    procedure clearspparams();
    //清除存储过程参数,包括in\out参数

    procedure addspparam_in(pparam : PSPParam); overload;
    procedure addspparam_in(name : string; value : Variant); overload;
    //增加输入参数

    procedure addspparam_out(pparam : PSPParam); overload;
    procedure addspparam_out(name : string; value : Variant); overload;
    //增加输出参数

    procedure getspparam(name : string; var value : Variant); overload;
    function getspparam(name : string) : Variant; overload;
    //取输出参数
  public
    procedure append(); virtual;
    //添加
    procedure update(); virtual;
    //修改
    procedure delete(); virtual;
    //删除
  published
    { 字段属性定义
    }
    property appendSpName : string read FAppendSpName write setAppendSpName;
    property updateSpName : string read FUpdateSpName write setUpdateSpName;
    property deleteSpName : string read FDeleteSpName write setDeleteSpName;


  { spcall相关
  }
  private
    FSpcall_spname : string;
    procedure setSPCallSpName(spname : String);

  public  
    function writeLog(caoz: string; yulzfc1:string='';yulzfc2:string='';yulzfc3: string=''): Boolean;
    //写日志

  public
    procedure addSpcallParam(name : string; value : Variant);
    procedure spcallPost();
    function getSpcallParamValue(name : string) : Variant;
  published
    property spcall_spname : string read FSpcall_spname write setSPCallSpName;
  end;

implementation

{$R *.dfm}

{ Tdmtblbase }

procedure Tdmtblbase.addspparam_in(name: string; value: Variant);
var
  p : PSPParam;
begin
  new(p);
  p^.name := name;
  p^.value := value;
  spparam_in.Add(p);
end;

procedure Tdmtblbase.addspparam_in(pparam: PSPParam);
var
  p : PSPParam;
begin
  new(p);
  p^.name := pparam.name;
  p^.value := pparam.value;
  spparam_in.Add(p);
end;

procedure Tdmtblbase.addspparam_out(pparam: PSPParam);
var
  p : PSPParam;
begin
  new(p);
  p^.name := pparam.name;
  p^.value := pparam.value;
  spparam_out.Add(p);
end;

procedure Tdmtblbase.addspparam_out(name: string; value: Variant);
var
  p : PSPParam;
begin
  new(p);
  p^.name := name;
  p^.value := value;
  spparam_out.Add(p);
end;

procedure Tdmtblbase.append;
begin
  //新增前的检查
  appendcheck;

  //新增前处理
  appendbefore;

  //新增提交
  appendpost;

  //新增提交后处理
  appendafter;
end;

procedure Tdmtblbase.appendafter;
begin
  { 存储过程标准的返回接口函数
  }
  addspparam_out('@ireturn', sp_append.Parameters.ParamValues['@ireturn']);
  addspparam_out('@sreturn', sp_append.Parameters.ParamValues['@sreturn']);
  addspparam_out('@scriptver', sp_append.Parameters.ParamValues['@scriptver']);
  addspparam_out('@bianm', sp_append.Parameters.ParamValues['@bianm']);
end;

procedure Tdmtblbase.appendbefore;
var
  i : Integer;
  p : PSPParam;
begin
  inherited;

  for i:=0 to spparam_in.Count-1 do
  begin
    p := spparam_in.Items[i];
    sp_append.Parameters.ParamByName(p^.name).Value := p^.value;
  end;
end;

procedure Tdmtblbase.appendcheck;
begin

end;

procedure Tdmtblbase.appendpost;
begin
  sp_append.ExecProc;
end;

procedure Tdmtblbase.clearspparams;
var
  i : Integer;
  p : PSPParam;
begin
  //sp_in
  for i:=0 to spparam_in.Count-1 do
  begin
    p := spparam_in.Items[i];
    Dispose(p);
  end;
  spparam_in.Clear;

  //sp_out
  for i:=0 to spparam_out.Count-1 do
  begin
    p := spparam_out.Items[i];
    Dispose(p);
  end;
  spparam_out.Clear;
end;

constructor Tdmtblbase.Create(pdllparams: PDllParam);
begin
  inherited Create(pdllparams); 

  //创建数据库检查对象
  dbcheck := tdbcheck.create(pdllparams^.padoconn);

  //创建存储过程输入\输出参数
  spparam_in := TList.Create;
  spparam_out := TList.Create;
end;

procedure Tdmtblbase.delete();
begin
  //删除前检查
  deletecheck;
  //删除前处理
  deletebefore;
  //删除提交
  deletepost;
  //删除后处理
  deleteafter;
end;

procedure Tdmtblbase.deleteafter;
begin
  { 存储过程标准的返回接口函数
  }
  addspparam_out('@ireturn', sp_delete.Parameters.ParamValues['@ireturn']);
  addspparam_out('@sreturn', sp_delete.Parameters.ParamValues['@sreturn']);
  addspparam_out('@scriptver', sp_delete.Parameters.ParamValues['@scriptver']);
end;

procedure Tdmtblbase.deletebefore;
var
  i : Integer;
  p : PSPParam;
begin
  inherited;

  for i:=0 to spparam_in.Count-1 do
  begin
    p := spparam_in.Items[i];
    sp_delete.Parameters.ParamByName(p^.name).Value := p^.value;
  end;
end;

procedure Tdmtblbase.deletecheck;
begin

end;

procedure Tdmtblbase.deletepost;
begin
  sp_delete.ExecProc;
end;

destructor Tdmtblbase.Destroy;
begin
  { 销毁存储过程输入\输出参数
  }
  //释放参数
  clearspparams;
  
  spparam_in.Free;
  spparam_in := nil;

  spparam_out.Free;
  spparam_out := nil;

  //释放数据库检查对象
  FreeAndNil(dbcheck);
  
  inherited;
end;

procedure Tdmtblbase.getspparam(name: string; var value: Variant);
var
  i : Integer;
begin
  for i:=0 to spparam_out.Count-1 do
  begin
    if UpperCase((PSPParam(spparam_out.Items[i]))^.name) = UpperCase(name) then
    begin
      value := (PSPParam(spparam_out.Items[i]))^.value;
      Exit;
    end;
  end;

  raise Exception.Create('参数' + name + '未找到!');
end;

function Tdmtblbase.getspparam(name: string): Variant;
var
  i : Integer;
begin
  for i:=0 to spparam_out.Count-1 do
  begin
    if UpperCase((PSPParam(spparam_out.Items[i]))^.name) = UpperCase(name) then
    begin
      { 2012-7-24
        当用户级别的错误消息参数@sreturn is null的时候, 尝试获取db层的错误消息, 这个问题是
        set XACT_ABORT on后db自动终止存储过程引起;
      }
      if (SameText(UpperCase(name), '@SRETURN')) and
         ((PSPParam(spparam_out.Items[i]))^.value = null) then
      begin
        if sp_append.Connection.Errors.Count > 0 then
        begin
          Result := sp_append.Connection.Errors[0].Get_Description();
        end
        else
        begin
          Result := '用户层和db层错误消息都未找到,请检查程序版本或联系客服！';
        end;
      end
      else
      begin
        Result := (PSPParam(spparam_out.Items[i]))^.value;
      end;

      Exit;
    end;
  end;

  raise Exception.Create('参数' + name + '未找到!');
end;

procedure Tdmtblbase.update();
begin
  //修改前检查
  updatecheck;
  //修改前检查
  updatebefore;
  //修改提交
  updatepost;
  //修改后处理
  updateafter;
end;

procedure Tdmtblbase.updateafter;
begin
  { 存储过程标准的返回接口函数
  }
  addspparam_out('@ireturn', sp_update.Parameters.ParamValues['@ireturn']);
  addspparam_out('@sreturn', sp_update.Parameters.ParamValues['@sreturn']);
  addspparam_out('@scriptver', sp_update.Parameters.ParamValues['@scriptver']);
end;

procedure Tdmtblbase.updatebefore;
var
  i : Integer;
  p : PSPParam;
begin
  inherited;

  for i:=0 to spparam_in.Count-1 do
  begin
    p := spparam_in.Items[i];
    sp_update.Parameters.ParamByName(p^.name).Value := p^.value;
  end;
end;

procedure Tdmtblbase.updatecheck;
begin

end;

procedure Tdmtblbase.updatepost;
begin
  sp_update.ExecProc;
end;

procedure Tdmtblbase.DataModuleCreate(Sender: TObject);
begin
  inherited;

  baseobj := tbase.Create;
end;

procedure Tdmtblbase.DataModuleDestroy(Sender: TObject);
begin
  inherited;

  baseobj.Free;
  baseobj := nil;
end;

procedure Tdmtblbase.ExecuteSProedure(psp: PUpAdoStoreProc; bWithBianm : Boolean);
var
  i : Integer;
  p : PSPParam;
begin
  inherited;

  //输入参数赋值给sp
  for i:=0 to spparam_in.Count-1 do
  begin
    p := spparam_in.Items[i];
    psp^.Parameters.ParamByName(p^.name).Value := p^.value;
  end;

  //执行sp
  psp^.ExecProc;

  //从sp取回返回值
  if bWithBianm then
    addspparam_out('@bianm', psp^.Parameters.ParamValues['@bianm']);

  addspparam_out('@ireturn', psp^.Parameters.ParamValues['@ireturn']);
  addspparam_out('@sreturn', psp^.Parameters.ParamValues['@sreturn']);
  addspparam_out('@scriptver', psp^.Parameters.ParamValues['@scriptver']);
end;

procedure Tdmtblbase.setAppendSpName(spname: string);
begin
  sp_append.ProcedureName := spname;
  sp_append.Parameters.Refresh;
  sp_append.Prepared := true;
end;

procedure Tdmtblbase.setDeleteSpName(spname: string);
begin
  sp_delete.ProcedureName := spname;
  sp_delete.Parameters.Refresh;
  sp_delete.Prepared := true;
end;

procedure Tdmtblbase.setUpdateSpName(spname: string);
begin
  sp_update.ProcedureName := spname;
  sp_update.Parameters.Refresh;
  sp_update.Prepared := true;
end;

procedure Tdmtblbase.setSPCallSpName(spname: String);
begin
  //                       
  //spcall.Parameters.Clear;
  spcall.ProcedureName := spname;
  spcall.Parameters.Refresh;
  //spcall.Prepared := true;
end;

procedure Tdmtblbase.addSpcallParam(name: string; value: Variant);
begin
  spcall.Parameters.ParamByName(name).Value := value;
end;

procedure Tdmtblbase.spcallPost;
begin
  spcall.ExecProc();
end;

function Tdmtblbase.getSpcallParamValue(name: string): Variant;
begin
  result := spcall.Parameters.ParamByName(name).Value;
end;

function Tdmtblbase.writeLog(caoz, yulzfc1, yulzfc2,
  yulzfc3: string): Boolean;
begin
  try
    qrylog.ExecuteSql(Format('insert into sys_riz_caoz(chuangklm,caozybm' +
        ',caozyxm,caozsj,caoz,yulzfc1,yulzfc2,yulzfc3)' +
        ' values(''%s'',%d,''%s'',getdate(),''%s'',''%s'',''%s'',''%s'')',
        [LowerCase(self.ClassName),fpdllparams^.mysystem^.loginyuang.bianm,
         fpdllparams^.mysystem^.loginyuang.xingm, caoz, yulzfc1, yulzfc2, yulzfc3]));
    Result:= True;
  except
    on E:Exception do
    begin
      Result:= False;
      errmsg:= e.Message;
    end;
  end;
end;

end.
