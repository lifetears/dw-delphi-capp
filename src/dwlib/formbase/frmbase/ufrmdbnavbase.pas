//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  ufrmdbnavbase.pas     
//                                          
//  创建: changym by 2011-11-19 
//        changup@qq.com                    
//  功能说明:                                            
//      带数据集导航条的数据感知窗体;
//  修改历史:
//                                            
//  版权所有 (C) 2011 by changym
//                                                       
//*******************************************************************
unit ufrmdbnavbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmmdichildbase, DB, ADODB, Buttons, UpSpeedButton, ExtCtrls,
  CnHint, udmtblbase, UpDataSource, frxDesgn, frxClass, frxDBSet, Menus,
  UpPopupMenu, UpAdoTable, UpAdoQuery, UpAdoStoreProc, ComCtrls,
  frxExportXLS, frxExportPDF, ImgList, RzButton, RzPanel, StdCtrls;

type
  Tfrmdbnavbase = class(Tfrmmdichildbase)
    sp_tbl_delete_check: TUpAdoStoreProc;
    tbmain: TRzToolbar;
    imglsttoolbar: TImageList;
    txttopspace: TStaticText;
    btnclose: TRzToolButton;
    procedure btncloseClick(Sender: TObject);
  private
  public
  end;

implementation

{$R *.dfm}

procedure Tfrmdbnavbase.btncloseClick(Sender: TObject);
begin
  inherited;

  Close;
end;

end.
