//*******************************************************************
//  pfzprogroup  ptablemanage   
//  ufrmdbdialogbase.pas     
//                                          
//  创建: changym by 2012-02-10 
//        changup@qq.com                     
//  功能说明:
//      ShowModal模式显示的对话框基类
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************

unit ufrmdbdialogbase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbbase, DB, ADODB, ExtCtrls, UPanel, Buttons, UpSpeedButton,
  frxClass, frxDesgn, frxDBSet, UpDataSource, Menus, UpPopupMenu,
  UpAdoTable, UpAdoQuery, frxExportXLS, frxExportCSV, frxExportPDF, Grids,
  Wwdbigrd, Wwdbgrid, UpWWDbGrid;

type
  Tfrmdbdialogbase = class(Tfrmdbbase)
    pfrmtop: TUPanel;
    bvl1: TBevel;
    btnyes: TUpSpeedButton;
    btnquit: TUpSpeedButton;
    procedure btnyesClick(Sender: TObject);
    procedure btnquitClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private

  public
  
  end;

var
  frmdbdialogbase: Tfrmdbdialogbase;

implementation

{$R *.dfm}

procedure Tfrmdbdialogbase.btnyesClick(Sender: TObject);
begin
  inherited;
  
  ModalResult := mrOk;
end;

procedure Tfrmdbdialogbase.btnquitClick(Sender: TObject);
begin
  inherited;

  ModalResult := mrCancel;
end;

procedure Tfrmdbdialogbase.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;

  //esc关闭窗口
  if Ord(Key) = VK_ESCAPE then
    ModalResult := mrCancel;
end;

end.
